import board
import neopixel
import time

import adafruit_fancyled.adafruit_fancyled as fancy

print("basic_fade_with_mix")

BRIGHTNESS = 0.4

rgb = neopixel.NeoPixel(board.NEOPIXEL, 10, brightness=1.0, auto_write=False)

RED = fancy.CRGB(255, 0, 0)
BLACK = fancy.CRGB(0, 0, 0)

point = 0.0
increment = 0.05

while True:
    color = fancy.gamma_adjust(fancy.mix(RED, BLACK, point), brightness=BRIGHTNESS)

    point += increment

    if point >= 1.0 or point <= 0.0:
        increment *= -1

    rgb.fill(color.pack())
    rgb.write()