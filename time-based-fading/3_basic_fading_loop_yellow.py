import board
import neopixel
import time

print("basic_fading_loop_yellow")

BRIGHTNESS = 1.0

rgb = neopixel.NeoPixel(board.NEOPIXEL, 10, brightness=BRIGHTNESS, auto_write=False)

color = [0, 0, 0]

increment = 12

while True:
    if color[0] <= 0:
        color[0] = 0
        color[1] = 0
        increment = 12

    if color[0] >= 255:
        color[0] = 255
        color[1] = 255
        increment = -12

    rgb.fill(color)
    rgb.write()

    color[0] += increment
    color[1] += increment

    time.sleep(0.3)