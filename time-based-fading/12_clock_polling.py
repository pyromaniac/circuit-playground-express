import board
import time

print("clock_polling")

checkin = time.monotonic()
counter = 0
seconds = 0

while True:
    counter += 1
    if time.monotonic() - checkin > 1.0:
        seconds += 1
        print("(approx)", seconds, "seconds have elapsed.", counter, "loops")
        checkin = time.monotonic()
        counter = 0