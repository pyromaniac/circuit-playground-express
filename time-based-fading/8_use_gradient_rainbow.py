import board
import neopixel
import time

print("use_gradient_rainbow")

rgb = neopixel.NeoPixel(board.NEOPIXEL, 10, brightness=1.0, auto_write=False)

gradient =  (6684672, 6684672, 6685184, 6686720, 6689024, 6692864, 6697984, 6705152, 5268992, 2123264, 550400, 26112, 17920, 6664, 1312, 80, 102, 131174, 458854, 1114214, 2031668, 3211281, 4718594, 6684672)

index = 0

while True:
    rgb.fill(gradient[index])
    rgb.write()

    index += 1
    if index > len(gradient)-1:
        index = 0

    time.sleep(0.2)