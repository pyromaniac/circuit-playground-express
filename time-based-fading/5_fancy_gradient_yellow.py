import board
import neopixel
import time

import adafruit_fancyled.adafruit_fancyled as fancy

print("fancy_gradient_yellow")

BRIGHTNESS = 0.4

rgb = neopixel.NeoPixel(board.NEOPIXEL, 10, brightness=1.0, auto_write=False)

BLACK = fancy.CRGB(0, 0, 0)
YELLOW = fancy.CRGB(255, 255, 0)

gradient = fancy.expand_gradient([
    (0.0, BLACK),
    (0.5, YELLOW),
    (1.0, BLACK)
], 20)

index = 0

while True:
    color = gradient[index]
    adjusted = fancy.gamma_adjust(color, brightness=BRIGHTNESS)
    rgb.fill(adjusted.pack())
    rgb.write()

    index += 1
    if index > len(gradient)-1:
        index = 0

    time.sleep(0.3)