import board
import neopixel
import time

import adafruit_fancyled.adafruit_fancyled as fancy

print("basic_fading_loop_gamma")

BRIGHTNESS = 0.4

rgb = neopixel.NeoPixel(board.NEOPIXEL, 10, brightness=1.0, auto_write=False)

color = [0, 0, 0]

increment = 12

while True:
    if color[0] <= 0:
        color[0] = 0
        color[1] = 0
        increment = 12

    if color[0] >= 255:
        color[0] = 255
        color[1] = 255
        increment = -12

    adjusted = fancy.gamma_adjust(fancy.CRGB(*color), brightness=BRIGHTNESS)
    rgb.fill(adjusted.pack())
    rgb.write()

    color[0] += increment
    color[1] += increment

    time.sleep(0.3)