import board
import neopixel
import time
from digitalio import DigitalInOut, Direction, Pull

print("fader_with_mode")

rgb = neopixel.NeoPixel(board.NEOPIXEL, 10, brightness=1.0, auto_write=False)

class Button:
    _debounce = 0.1

    def __init__(self, pin, name, onrelease):
        self.name = name
        self.checkin = time.monotonic()
        self.state = False
        self.onrelease = onrelease
        self.input = DigitalInOut(pin)
        self.input.direction = Direction.INPUT
        self.input.pull = Pull.DOWN

    def press(self):
        print(self.name, "pressed")

    def release(self):
        print(self.name, "released")
        self.onrelease()

    def check(self):
        return self.input.value

    def update(self):
        if time.monotonic() - self.checkin > self._debounce:
            if self.state and not self.check():
                self.release()

            if not self.state and self.check():
                self.press()

            self.state = self.check()

            self.checkin = time.monotonic()

class Fader:
    def __init__(self, palette, interval=0.1):
        self.checkin = time.monotonic()
        self.color = 0
        self.interval = interval
        self.palette = palette
        self.max = len(self.palette)*interval
        self.epoch = 0
        self.index = 0

    def update(self):
        self.epoch = time.monotonic() - self.checkin

        self.index = round((self.epoch%self.max)/self.interval)

        if self.index > len(self.palette)-1:
            self.index = 0
            self.checkin = time.monotonic()

        self.color = self.palette[self.index]

class ModeFader(Fader):
    def __init__(self, palette, interval=0.1):
        self.on = True
        Fader.__init__(self, palette, interval)

    def update(self):
        if not self.on:
            self.color = 0
            return

        Fader.update(self)

class AutoOffFader(ModeFader):
    def reset(self):
        self.epoc = 0
        self.checkin = time.monotonic()
        self.on = True
        self.index = 0

    def update(self):
        ModeFader.update(self)
        if self.on and self.index == len(self.palette)-1:
            self.on = False

green_to_off = (19456, 15360, 11520, 8448, 6144, 4096, 2560, 1536, 768, 256, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
pride = (4980736, 4980736, 4981248, 4982272, 4984064, 4986880, 4990720, 4996096, 3951616, 1592320, 412672, 19456, 13312, 5126, 1048, 60, 76, 65612, 327756, 852044, 1507367, 2359309, 3538946, 4980736)

auto_off = AutoOffFader(green_to_off, 0.05)
runner = ModeFader(pride, 0.1)

def fire_auto():
    runner.on = False
    auto_off.reset()

def cycle_toggle():
    auto_off.on = False
    runner.on = not runner.on

button1 = Button(board.D4, "a", fire_auto)
button2 = Button(board.D5, "b", cycle_toggle)

previous = None

while True:
    if auto_off.on:
        fader = auto_off
    if runner.on:
        fader = runner

    button1.update()
    button2.update()
    fader.update()

    if fader.color != previous:
        rgb.fill(fader.color)
        rgb.write()
        previous = fader.color