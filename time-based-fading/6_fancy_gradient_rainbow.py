import board
import neopixel
import time

import adafruit_fancyled.adafruit_fancyled as fancy

print("fancy_gradient_rainbow")

BRIGHTNESS = 0.4

rgb = neopixel.NeoPixel(board.NEOPIXEL, 10, brightness=1.0, auto_write=False)

WHITE = fancy.CRGB(255, 255, 255)
BLACK = fancy.CRGB(0, 0, 0)
RED = fancy.CRGB(255, 0, 0)
GREEN = fancy.CRGB(0, 255, 0)
YELLOW = fancy.CRGB(255, 255, 0)
BLUE = fancy.CRGB(0, 0, 255)
ORANGE = fancy.CRGB(255, 127, 0)
VIOLET = fancy.CRGB(139, 0, 255)

gradient = fancy.expand_gradient([
    (0.0, RED),
    (0.16, ORANGE),
    (0.33, YELLOW),
    (0.5, GREEN),
    (0.66, BLUE),
    (0.82, VIOLET),
    (1.0, RED)
], 24)

index = 0

while True:
    color = gradient[index]
    adjusted = fancy.gamma_adjust(color, brightness=BRIGHTNESS)
    rgb.fill(adjusted.pack())
    rgb.write()

    index += 1
    if index > len(gradient)-1:
        index = 0

    time.sleep(0.3)