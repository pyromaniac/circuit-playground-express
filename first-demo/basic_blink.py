# this came directly from Adafruit's guide at
# https://learn.adafruit.com/adafruit-circuit-playground-express/creating-and-editing-code

print("Hello World!")

import board
import digitalio
import time

led = digitalio.DigitalInOut(board.LED)
led.direction = digitalio.Direction.OUTPUT

while True:
    led.value = True
    time.sleep(0.5)
    led.value = False
    time.sleep(0.5)