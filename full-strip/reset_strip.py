
print("resetting the first 50...")

# Drive NeoPixels on the NeoPixels Block on Crickit for
#  Circuit Playground Express
import time
import neopixel
import board
import adafruit_fancyled.adafruit_fancyled as fancy

num_pixels = 120  # Number of pixels driven from Crickit NeoPixel terminal

# The following line sets up a NeoPixel strip on Crickit CPX pin A1
pixels = neopixel.NeoPixel(board.A1, num_pixels, brightness=0.3,
                           auto_write=False)

OFF = (0, 0, 0)

print("filling with off")
pixels.fill(OFF)
pixels.show()

print("done filling, loop forever")

while True:
    time.sleep(60)
    print("still sleeping...")
